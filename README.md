# wall-splash

A [Sails](http://sailsjs.org) application to display aesthetic wallpapers.

![Demo](./assets/images/demo.gif)