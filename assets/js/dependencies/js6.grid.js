document.addEventListener('DOMContentLoaded', function () {
  const dragContainer = document.querySelector('.drag-container');
  const gridElement = document.querySelector('.grid');
  const filterField = document.querySelector('.grid-control-field.filter-field');
  const searchField = document.querySelector('.grid-control-field.search-field');
  const sortField = document.querySelector('.grid-control-field.sort-field');
  const layoutField = document.querySelector('.grid-control-field.layout-field');
  const itemTemplate = document.querySelector('.grid-item-template');

  let numberOfWallpapers = 0;
  let dragOrder = [];
  let sortFieldValue;
  let searchFieldValue;

  //
  // GRID
  //

  const grid = new Muuri(gridElement, {
    items: createItemElements(),
    showDuration: 400,
    showEasing: 'ease',
    hideDuration: 400,
    hideEasing: 'ease',
    layoutDuration: 400,
    layoutEasing: 'cubic-bezier(0.625, 0.225, 0.100, 0.890)',
    sortData: {
      title(item, element) {
        return element.getAttribute('data-title') || '';
      },
      color(item, element) {
        return element.getAttribute('data-color') || '';
      },
      uploadedOn(item, element) {
        return element.getAttribute('data-uploadedOn') || '';
      },
    },
    dragEnabled: true,
    dragHandle: '.grid-card-handle',
    dragContainer: dragContainer,
    dragRelease: {
      duration: 400,
      easing: 'cubic-bezier(0.625, 0.225, 0.100, 0.890)',
      useDragContainer: true,
    },
    dragPlaceholder: {
      enabled: true,
      createElement(item) {
        return item.getElement().cloneNode(true);
      },
    },
    dragAutoScroll: {
      targets: [window],
      sortDuringScroll: false,
      syncAfterScroll: false,
    },
  });

  window.grid = grid;

  numberOfWallpapers = grid.getItems().length

  //
  // Grid helper functions
  //

  function initGrid() {
    // Reset field values.
    searchField.value = '';
    [sortField, filterField, layoutField].forEach((field) => {
      field.value = field.querySelectorAll('option')[0].value;
    });

    // Set inital search query, active filter, active sort value and active layout.
    searchFieldValue = searchField.value.toLowerCase();
    sortFieldValue = sortField.value;

    updateDragState();

    // Search field binding.
    searchField.addEventListener('keyup', function () {
      var newSearch = searchField.value.toLowerCase();
      if (searchFieldValue !== newSearch) {
        searchFieldValue = newSearch;
        filter();
      }
    });

    // Filter, sort and layout bindings.
    filterField.addEventListener('change', filter);
    sortField.addEventListener('change', sort);
    layoutField.addEventListener('change', updateLayout);

    // Double click to zoom in/out
    gridElement.addEventListener('dblclick', (e) => {
      let gridItem = e.target;

      if (gridItem.matches('.grid-item-content') || gridItem.matches('.grid-card')
        || gridItem.matches('.grid-card-title') || gridItem.matches('.grid-card-image')) {
        gridItem = gridItem.closest('.grid-item');
        e.stopPropagation();
      } else if (gridItem.matches('.grid-card-handle')) {
        return
      }

      if (gridItem.classList.contains('full')) {
        gridItem.classList.remove("full");
      } else {
        gridItem.classList.add("full");
      }

      let index = grid.getItems().indexOf(grid.getItem(gridItem));

      if (index % 2 != 0) {
        grid.move(index - 1, index);
      }

      grid.refreshItems();
      grid.layout();
    });

    // To delete items
    gridElement.addEventListener('click', (e) => {
      if (e.target.matches('.grid-card-remove')) {
        removeItem(e.target.closest('.grid-item'));
      }
    });
  }

  function filter(onFinish = null) {
    const filterFieldValue = filterField.value;
    let body = document.querySelector('body');
    console.log(filterFieldValue)
    switch (filterFieldValue) {
      case '':
        body.className = 'zigzagbg-light';
        break;
      case 'nature':
        body.className = 'topologybg';
        break;
      case 'futuristic':
        body.className = 'digitalbg';
        break;
      case 'space':
        body.className = 'bubblesbg';
        break;
      case 'architecture':
        body.className = 'graphbg';
        break;
      default:
        break;
    }
    grid.filter(
      (item) => {
        const element = item.getElement();
        const isSearchMatch =
          !searchFieldValue ||
          (element.getAttribute('data-title') || '').toLowerCase().indexOf(searchFieldValue) > -1;
        const isFilterMatch =
          !filterFieldValue || filterFieldValue === element.getAttribute('data-color');
        return isSearchMatch && isFilterMatch;
      },
      { onFinish: onFinish }
    );
  }

  function sort() {
    var currentSort = sortField.value;
    if (sortFieldValue === currentSort) return;

    updateDragState();

    // If we are changing from "order" sorting to something else
    // let's store the drag order.
    if (sortFieldValue === 'order') {
      dragOrder = grid.getItems();
    }

    // Sort the items.
    switch (currentSort) {
      case 'title':
        grid.sort('title');
        break;
      case 'uploadedOn':
        grid.sort('uploadedOn');
        break;
      case 'color':
        grid.sort('color title');
        break;
      default:
        grid.sort(dragOrder);
        break;
    }

    // Update active sort value.
    sortFieldValue = currentSort;
  }

  function removeItem(gridItem) {

    let item = grid.getItem(gridItem);
    let id = gridItem.id;

    if (!item) return;

    grid.hide([item], {
      onFinish: async () => {

        let deleteResponse = await fetch('/wallpapers/delete', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json;charset=utf-8'
          },
          body: JSON.stringify({ id: id })
        });

        if (deleteResponse.status == 200) {
          grid.remove(item, { removeElements: true });
          if (sortFieldValue !== 'order') {
            const itemIndex = dragOrder.indexOf(item);
            if (itemIndex > -1) dragOrder.splice(itemIndex, 1);
          }
          
          numberOfWallpapers--;

          if (numberOfWallpapers === 0) {
            window.location.reload();
          }
        }
        else {
          $('.toast').toast('show');
          grid.show([item])
        }
      },
    });
  }
  
  function updateLayout() {
    const { layout } = grid._settings;
    const val = layoutField.value;
    layout.alignRight = val.indexOf('right') > -1;
    layout.alignBottom = val.indexOf('bottom') > -1;
    layout.fillGaps = val.indexOf('fillgaps') > -1;
    grid.layout();
  }

  function updateDragState() {
    if (sortField.value === 'order') {
      gridElement.classList.add('drag-enabled');
    } else {
      gridElement.classList.remove('drag-enabled');
    }
  }

  //
  // Generic helper functions
  //

  function createItemElements() {
    const elements = [];
    const data = document.querySelectorAll('data');

    data.forEach(function (node) {
      let wallpaper = {
        title: node.getAttribute('title'),
        image: node.getAttribute('image'),
        category: node.getAttribute('category'),
        uploadedOn: node.getAttribute('uploadedOn'),
        id: node.id
      };
      elements.push(createItemElement(wallpaper));
    });

    return elements;
  }

  function createItemElement(wallpaper) {
    const color = wallpaper.category;
    const width = 2;
    const height = 2;
    const itemElem = document.importNode(itemTemplate.content.children[0], true);

    itemElem.classList.add('h' + height, 'w' + width, color);
    itemElem.setAttribute('data-color', color);
    itemElem.setAttribute('data-title', wallpaper.title);
    itemElem.setAttribute('data-uploadedOn', wallpaper.uploadedOn);
    itemElem.querySelector('img').src = wallpaper.image;
    itemElem.querySelector('.grid-card-title').innerHTML = wallpaper.title;
    itemElem.id = wallpaper.id;

    return itemElem;
  }

  //
  // Fire it up!
  //

  initGrid();
});
