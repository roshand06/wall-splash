let body = document.body;
let pagename = document.querySelector('span.pagename').id;

switch (pagename) {
    case 'homepage':
        body.className = "zigzagbg";
        break;

    case 'about':
        body.className = "zigzagbg";
        break;

    case 'empty':
        body.className = "zigzagbg";
        break;

    case 'explore':
        body.className = "zigzagbg-light";
        break;

    case 'add':
        body.className = "zigzagbg";
        break;
    default:
        break;
}