/**
 * WallpapersController
 *
 * @description :: Server-side logic for managing wallpapers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    explore: function (req, res) {
        Wallpapers.find({}).exec(function (err, wallpapers) {
            if (err) {
                res.send(500, 'Database error');
            }
            if (wallpapers.length === 0) {
                res.view('empty');
            } else {
                res.view('explore', { wallpapers: wallpapers });
            }
        });
    },
    add: function (req, res) {
        res.view('add');
    },
    insert: function (req, res) {
        const title = req.body.title;
        const image = req.body.url;
        const category = req.body.category;

        Wallpapers.create({
            title: title,
            image: image,
            category: category,
            uploadedOn: new Date()
        }).exec(function (err) {
            if (err) {
                console.log(err);
                res.send(500, 'Database error');
            } else {
                res.redirect('/wallpapers/explore');
            }
        });
        return false;
    },
    delete: function(req, res) {
        const id = req.body.id;

        Wallpapers.destroy({ id: id }).exec(function (err) {
            if (err) {
                console.log(err);
                res.send(500, 'Database error');
            } else {
                res.send(200);
            }
        });
    }
};

